import math
import shutil
import subprocess
import unittest
from datetime import timedelta
from pathlib import Path
from tempfile import NamedTemporaryFile

import pydantic

from audio_sam import audio_library
from audio_sam.audio_library import audio_file_information, progress_from_ffmpeg_output


class TestAudioFunctions(unittest.TestCase):
    def test_duration_parser(self):
        self.assertEqual(audio_library.duration_parser("00:00:00"), timedelta(seconds=0))
        self.assertEqual(audio_library.duration_parser("00:01:02"), timedelta(minutes=1, seconds=2))
        self.assertEqual(audio_library.duration_parser("01:02:03"), timedelta(hours=1, minutes=2, seconds=3))
        self.assertEqual(
            audio_library.duration_parser("01:02:03.345678"),
            timedelta(hours=1, minutes=2, seconds=3, microseconds=345678),
        )
        self.assertEqual(
            audio_library.duration_parser("1:02:03.45"), timedelta(hours=1, minutes=2, seconds=3, microseconds=450_000)
        )
        self.assertEqual(
            audio_library.duration_parser("02:03.45"), timedelta(minutes=2, seconds=3, microseconds=450_000)
        )
        self.assertEqual(audio_library.duration_parser("03.45"), timedelta(seconds=3, microseconds=450_000))
        self.assertEqual(audio_library.duration_parser("03,45"), timedelta(seconds=3, microseconds=450_000))

    def test_duration(self):
        truth = timedelta(minutes=1, seconds=30, microseconds=174750)
        self.assertEqual(truth, audio_library.duration("./audio_file_1.mp3"))

    def test_audio_info(self):
        truth = {"bit_rate": 32_000, "duration": timedelta(minutes=1, seconds=30, microseconds=174750)}

        self.assertDictEqual(truth, audio_file_information("./audio_file_1.mp3"))


class TestAudioSequence(unittest.TestCase):
    def setUp(self):
        self.filename = str(Path("./audio_file_1.mp3").absolute())
        self.test_sequence = audio_library.AudioSequence(filename=self.filename)

    def test_init_simple(self):
        self.assertEqual(self.test_sequence.filename, self.filename)
        self.assertEqual(self.test_sequence.start_time, None)
        self.assertEqual(self.test_sequence.end_time, None)
        self.assertEqual(self.test_sequence.file_duration, timedelta(minutes=1, seconds=30, microseconds=174750))

    def test_init_with_missing_file(self):
        with self.assertRaises(pydantic.ValidationError):
            audio_library.AudioSequence(filename="not_exists.mp3")

    def test_ffmpeg_args(self):
        self.assertListEqual(["-i", f"{self.filename}"], self.test_sequence.to_ffmpeg_args())

        self.assertListEqual(
            ["-ss", "0:00:32", "-i", f"{self.filename}"],
            audio_library.AudioSequence(
                filename=self.filename, start_time=timedelta(minutes=0, seconds=32)
            ).to_ffmpeg_args(),
        )

        self.assertListEqual(
            ["-to", "0:01:15", "-i", f"{self.filename}"],
            audio_library.AudioSequence(
                filename=self.filename, end_time=timedelta(minutes=1, seconds=15)
            ).to_ffmpeg_args(),
        )

        self.assertListEqual(
            ["-ss", "0:00:15", "-to", "0:01:15", "-i", f"{self.filename}"],
            audio_library.AudioSequence(
                filename=self.filename, start_time=timedelta(seconds=15), end_time=timedelta(seconds=75)
            ).to_ffmpeg_args(),
        )

    def test_sequence_duration(self):
        test_sequence = audio_library.AudioSequence(filename=self.filename)
        self.assertEqual(test_sequence.sequence_duration, timedelta(minutes=1, seconds=30, microseconds=174750))

        test_sequence = audio_library.AudioSequence(filename=self.filename, end_time=timedelta(minutes=1))
        self.assertEqual(test_sequence.sequence_duration, timedelta(minutes=1))

        test_sequence = audio_library.AudioSequence(filename=self.filename, start_time=timedelta(minutes=1))
        self.assertEqual(test_sequence.sequence_duration, timedelta(seconds=30, microseconds=174750))

        test_sequence = audio_library.AudioSequence(
            filename=self.filename, start_time=timedelta(seconds=30), end_time=timedelta(minutes=1)
        )
        self.assertEqual(test_sequence.sequence_duration, timedelta(seconds=30))

    def test_file_duration(self):
        test_sequence = audio_library.AudioSequence(filename=self.filename)
        self.assertEqual(test_sequence.file_duration, timedelta(minutes=1, seconds=30, microseconds=174750))
        self.assertEqual(test_sequence.file_duration, test_sequence.sequence_duration)

    def test_bit_rate(self):
        test_sequence = audio_library.AudioSequence(filename=self.filename)
        self.assertEqual(test_sequence.bit_rate, 32_000)


class TestMerging(unittest.TestCase):
    def test_ffmpeg_input_args(self):
        audio_files = ("audio_file_1.mp3", "audio_file 2.mp3", "audio_file_3.mp3")
        audio_sequence_list = [audio_library.AudioSequence(filename=x) for x in audio_files]
        ffmpeg_arg_str = audio_library.audio_sequences_to_ffmpeg_args(audio_sequence_list)

        self.assertListEqual(
            ["-i", "audio_file_1.mp3", "-i", "audio_file 2.mp3", "-i", "audio_file_3.mp3"], ffmpeg_arg_str
        )

        audio_sequence_list = [
            audio_library.AudioSequence(filename=x, start_time=timedelta(seconds=5), end_time=timedelta(seconds=10))
            for x in audio_files
        ]
        ffmpeg_arg_str = audio_library.audio_sequences_to_ffmpeg_args(audio_sequence_list)
        self.assertListEqual(
            [
                "-ss",
                "0:00:05",
                "-to",
                "0:00:10",
                "-i",
                "audio_file_1.mp3",
                "-ss",
                "0:00:05",
                "-to",
                "0:00:10",
                "-i",
                "audio_file 2.mp3",
                "-ss",
                "0:00:05",
                "-to",
                "0:00:10",
                "-i",
                "audio_file_3.mp3",
            ],
            ffmpeg_arg_str,
        )

        audio_sequence_list = [
            audio_library.AudioSequence(
                filename="audio_file_1.mp3", start_time=timedelta(seconds=30), end_time=timedelta(minutes=1)
            ),
            audio_library.AudioSequence(filename="audio_file 2.mp3", start_time=timedelta(seconds=5)),
            audio_library.AudioSequence(filename="audio_file_3.mp3", end_time=timedelta(seconds=10)),
        ]

        ffmpeg_arg_str = audio_library.audio_sequences_to_ffmpeg_args(audio_sequence_list)
        self.assertListEqual(
            [
                "-ss",
                "0:00:30",
                "-to",
                "0:01:00",
                "-i",
                "audio_file_1.mp3",
                "-ss",
                "0:00:05",
                "-i",
                "audio_file 2.mp3",
                "-to",
                "0:00:10",
                "-i",
                "audio_file_3.mp3",
            ],
            ffmpeg_arg_str,
        )

    def test_ffmpeg_cmd(self):
        audio_files = ("audio_file_1.mp3", "audio_file 2.mp3", "audio_file_3.mp3")
        audio_sequence_list = [audio_library.AudioSequence(filename=x) for x in audio_files]

        with self.subTest(msg="Global options - hide_banner"):
            ffmpeg_cmd = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3", hide_banner=True)
            self.assertIn("-hide_banner", ffmpeg_cmd)

            ffmpeg_cmd = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3", hide_banner=False)
            self.assertNotIn("-hide_banner", ffmpeg_cmd)

        with self.subTest(msg="Global options - overwrite"):
            ffmpeg_cmd = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3", overwrite_output=True)
            self.assertIn("-y", ffmpeg_cmd)

            ffmpeg_cmd = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3", overwrite_output=False)
            self.assertNotIn("-y", ffmpeg_cmd)
            self.assertIn("-n", ffmpeg_cmd)

        with self.subTest(msg="Merge several files"):
            # Test command for several audio files
            ffmpeg_cmd = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3")
            truth = [
                f'{shutil.which('ffmpeg')}',
                "-hide_banner",
                "-y",
                "-i",
                "audio_file_1.mp3",
                "-i",
                "audio_file 2.mp3",
                "-i",
                "audio_file_3.mp3",
                "-filter_complex",
                "[0:a][1:a][2:a]concat=n=3:v=0:a=1",
                "out.mp3",
            ]
            self.assertListEqual(truth, ffmpeg_cmd)

        with self.subTest(msg="Single files"):
            # Test command for single audio file
            ffmpeg_cmd = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list[:1], "out.mp3")
            truth = [
                f'{shutil.which('ffmpeg')}',
                "-hide_banner",
                "-y",
                "-i",
                "audio_file_1.mp3",
                "-c:a",
                "copy",
                "out.mp3",
            ]
            self.assertListEqual(truth, ffmpeg_cmd)

    def test_ffmpeg_cmd_bitrate(self):
        audio_files = ("audio_file_1.mp3", "audio_file 2.mp3", "audio_file_3.mp3")
        audio_sequence_list = [audio_library.AudioSequence(filename=x) for x in audio_files]

        with self.subTest(msg="bit_rate=None"):
            ffmpeg_cmd_for_merging = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3", bit_rate=None)
            self.assertNotIn("-b:a", ffmpeg_cmd_for_merging)

        with self.subTest(msg="bit_rate=0"):
            ffmpeg_cmd_for_merging = audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, "out.mp3", bit_rate=0)
            self.assertIn("-b:a", ffmpeg_cmd_for_merging)
            ffmpeg_bit_rate_arg_index = ffmpeg_cmd_for_merging.index("-b:a")
            self.assertEqual(int(ffmpeg_cmd_for_merging[ffmpeg_bit_rate_arg_index + 1]), 32_000)

        with self.subTest(msg="bit_rate=196k"):
            ffmpeg_cmd_for_merging = audio_library.ffmpeg_cmd_for_merging(
                audio_sequence_list, "out.mp3", bit_rate="196k"
            )
            self.assertIn("-b:a", ffmpeg_cmd_for_merging)
            ffmpeg_bit_rate_arg_index = ffmpeg_cmd_for_merging.index("-b:a")
            self.assertEqual(ffmpeg_cmd_for_merging[ffmpeg_bit_rate_arg_index + 1], "196k")

    # Regression test, which is slow as it takes ~0.5 s.
    def test_ffmpeg_run(self):
        audio_sequence_list = [
            audio_library.AudioSequence(
                filename="audio_file_1.mp3", start_time=timedelta(seconds=30), end_time=timedelta(minutes=1)
            ),
            audio_library.AudioSequence(filename="audio_file 2.mp3", start_time=timedelta(seconds=5)),
            audio_library.AudioSequence(filename="audio_file_3.mp3", end_time=timedelta(seconds=10)),
        ]

        with self.subTest(msg="Multi file split and merge"):
            with NamedTemporaryFile(mode="w+b", suffix=".mp3", delete=True, delete_on_close=False) as out:
                result = subprocess.run(
                    audio_library.ffmpeg_cmd_for_merging(audio_sequence_list, out.name), capture_output=True, text=True
                )
                progress = [
                    progress_from_line
                    for line in result.stderr.strip().split("\n")
                    if (progress_from_line := progress_from_ffmpeg_output(line))
                ]

                self.assertTrue(Path(out.name).exists(), msg="merge audio file not created")

                # Check that duration is approximately the sum of the individual durations.
                self.assertLess(abs(50 - progress[-1].total_seconds()), 0.2)

        with self.subTest(msg="Single file trimming"):
            with NamedTemporaryFile(mode="w+b", suffix=".mp3", delete=True, delete_on_close=False) as out:
                result = subprocess.run(
                    audio_library.ffmpeg_cmd_for_merging(audio_sequence_list[:1], out.name),
                    capture_output=True,
                    text=True,
                )
                progress = [
                    progress_from_line
                    for line in result.stderr.strip().split("\n")
                    if (progress_from_line := progress_from_ffmpeg_output(line))
                ]

                self.assertTrue(Path(out.name).exists(), msg="merge audio file not created")

                # Check that duration is approximately the sum of the individual durations.
                self.assertLess(abs(30 - progress[-1].total_seconds()), 0.2)


class TestProgress(unittest.TestCase):
    def test_progress_from_ffmpeg_out(self):
        test_sequence = (
            ("ffmpeg version 6.1.1 Copyright (c) 2000-2023 the FFmpeg developers", None),
            ("size=       0kB time=N/A bitrate=N/A speed=N/A    ", None),
            ("size=     768kB time=00:02:03.95 bitrate=  50.8kbits/s speed= 247x", timedelta(seconds=123.95)),
            ("size=    1792kB time=00:04:07.69 bitrate=  59.3kbits/s speed= 247x", timedelta(seconds=247.69)),
        )

        for output, truth in test_sequence:
            with self.subTest(output=output, truth=truth):
                self.assertEqual(truth, audio_library.progress_from_ffmpeg_output(output))

    def test_ffmpeg_progress(self):
        test_sequence = [audio_library.AudioSequence(filename="audio_file_1.mp3")] * 2
        true_duration = timedelta(minutes=1, seconds=30, microseconds=174750) * 2

        progressor = audio_library.FFMpegProgressFraction(test_sequence)
        self.assertEqual(true_duration, progressor.merged_duration)

        test_lines = (
            ("ffmpeg version 6.1.1 Copyright (c) 2000-2023 the FFmpeg developers", float("nan")),
            ("size=       0kB time=N/A bitrate=N/A speed=N/A    ", float("nan")),
            (
                "size=     768kB time=00:00:15.95 bitrate=  50.8kbits/s speed= 247x",
                15.95 / true_duration.total_seconds(),
            ),
            (
                "size=    1792kB time=00:01:07.69 bitrate=  59.3kbits/s speed= 247x",
                67.69 / true_duration.total_seconds(),
            ),
        )

        for output, truth in test_lines:
            with self.subTest(output=output, truth=truth):
                if math.isnan(truth):
                    self.assertTrue(math.isnan(progressor(output)))
                else:
                    self.assertAlmostEqual(truth, progressor(output), places=5)


if __name__ == "__main__":
    unittest.main()
