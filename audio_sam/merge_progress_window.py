import io
import math
import pathlib
import subprocess
import threading
from datetime import timedelta
from typing import List

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib  # noqa: E402

from audio_library import FFMpegProgressFraction, AudioSequence, ffmpeg_cmd_for_merging  # noqa: E402


class MergeProgressWindow(Gtk.Window):
    def __init__(self, progress_fraction: FFMpegProgressFraction):
        super().__init__(title="Merging audio files", default_width=600, default_height=500)

        self.progress_fraction = progress_fraction

        box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.VERTICAL)

        self.progress_bar = Gtk.ProgressBar()

        self.textview = Gtk.TextView()
        self.textview.props.editable = False
        self.textview.props.cursor_visible = False
        self.textbuffer = self.textview.get_buffer()

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.set_vexpand(True)
        self.scrolled_window.set_hexpand(True)
        self.scrolled_window.set_child(self.textview)

        self.expander = Gtk.Expander()
        self.expander.set_hexpand(True)
        self.expander.set_vexpand(True)
        self.expander.set_child(self.scrolled_window)

        box.append(self.progress_bar)
        box.append(self.expander)

        hbox = Gtk.Box(spacing=12, valign=Gtk.Align.CENTER, halign=Gtk.Align.CENTER)
        self.stop_button = Gtk.Button.new_with_label("Stop")

        box.append(hbox)

        self.set_child(box)

        self.props.modal = True

    def update_progress_bar(self, progress, text=None):
        # Pulse the progress bar initially when progress is None or NaN
        if progress is None or math.isnan(progress):
            self.progress_bar.pulse()
        else:
            self.progress_bar.props.fraction = progress

        if text:
            self.progress_bar.props.text = text

    def update_textview(self, text):
        self.textbuffer.props.text = text

    def update_from_ffmpeg_line(self, line):
        fraction = self.progress_fraction(line)

        GLib.idle_add(self.update_ui, fraction, line)

    def update_ui(self, fraction, line):
        self.update_progress_bar(fraction)
        end_iter = self.textbuffer.get_end_iter()
        self.textbuffer.insert(end_iter, line)

        # Scroll to the end of the text
        end_mark = self.textbuffer.create_mark("end", end_iter, False)
        self.textview.scroll_mark_onscreen(end_mark)


class MergeWindow(MergeProgressWindow):
    def __init__(self, audio_sequence: List[AudioSequence], output: str):
        super().__init__(FFMpegProgressFraction(audio_sequence))

        self.ffmpeg_cmd = ffmpeg_cmd_for_merging(audio_sequence, output)

        self.popen_kwds = dict(bufsize=1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

        self.output = pathlib.Path(output)

        self.process = None
        self.capture = None
        self.merge_thread = None
        self.terminated = False

    def run(self):
        self.capture = io.StringIO()
        with subprocess.Popen(self.ffmpeg_cmd, **self.popen_kwds) as self.process:
            for line in self.process.stdout:
                self.capture.write(line)
                self.update_from_ffmpeg_line(line)

        if self.terminated:
            if self.output.exists():
                self.output.unlink()

        elif self.process.returncode != 0:
            dialog = Gtk.AlertDialog()
            dialog.set_message("Error while merging of files.")
            dialog.set_detail(
                f"Return Code: {self.process.returncode}\n\n"
                f"Args:\n\n{self.process.args}\n\n"
                f"Output:\n\n {self.capture.getvalue()}"
            )
            dialog.set_modal(True)
            dialog.set_buttons(["Ok"])
            dialog.choose(self)

        self.process = None
        self.capture = None

        if self.merge_thread is not None:
            self.merge_thread = None

        GLib.idle_add(self.close)

    def start(self):
        self.connect("close-request", self.stop)

        self.merge_thread = threading.Thread(target=self.run)
        self.merge_thread.damon = True
        self.merge_thread.start()

    def stop(self, *args):
        self.terminated = True

        if self.process is not None:
            self.process.terminate()


if __name__ == "__main__":
    audio_sequence = [
        AudioSequence(
            filename="../test/audio_file_1.mp3",
            start_time=timedelta(seconds=30),
            end_time=timedelta(minutes=1),
        ),
        AudioSequence(filename="../test/audio_file 2.mp3", start_time=timedelta(seconds=5)),
        AudioSequence(filename="../test/audio_file_3.mp3", end_time=timedelta(seconds=10)),
    ] * 20

    # window = MergeProgressWindow(FFMpegProgressFraction(audio_sequence))
    # window.present()
    #
    # thread = threading.Thread(
    #     target=merge_audio_sequences,
    #     args=(audio_sequence, "../test/output.mp3", window.update_from_ffmpeg_line),
    # )
    # thread.start()
    # result = merge_audio_sequences(audio_sequence, "../test/output.mp3", window.update_from_ffmpeg_line)

    window = MergeWindow(audio_sequence, "../test/output.mp3")
    window.present()

    window.start()

    while len(Gtk.Window.get_toplevels()) > 0:
        GLib.MainContext.default().iteration(True)
