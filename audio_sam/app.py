import sys

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import GLib, Gtk  # noqa: E402

from app_window import AudioSamWindow  # noqa: E402


class AudioSaMApp(Gtk.Application):
    def __init__(self):
        super().__init__(application_id="com.example.AudioSaM")
        GLib.set_application_name("Audio Split and Merge")
        self.window = None

    def do_activate(self):
        if self.window is None:
            self.window = AudioSamWindow(self)

        self.window.present()


def run_app(args: list[str] = None):
    if args is None:
        args = sys.argv

    app = AudioSaMApp()

    exit_status = app.run(args)
    sys.exit(exit_status)


if __name__ == "__main__":
    run_app(sys.argv)
