import functools
import re
import shutil
import subprocess
from datetime import timedelta
from pathlib import Path
from typing import Optional

import pydantic

FFMPEG_BINARY = shutil.which("ffmpeg")
FFPROBE_BINARY = shutil.which("ffprobe")

FFMPEG_PROGRESS_RE = re.compile(r"size=\s+(\d+)[kKi]+B time=([0-9:.]+)")


def duration_parser(ffmpeg_duration: str) -> timedelta:
    """
    Parse ffmpeg duration string and returns timedelta object
    Parameters
    ----------
    ffmpeg_duration

    Returns
    -------
        duration: timedelta
    """

    # Split into hour, minute and second part
    tokens = [float(x.replace(",", ".")) for x in ffmpeg_duration.split(":")]
    match tokens:
        case (hours, minutes, seconds):
            return timedelta(hours=hours, minutes=minutes, seconds=seconds)

        case (minutes, seconds):
            return timedelta(minutes=minutes, seconds=seconds)

        case (seconds,):
            return timedelta(seconds=seconds)

    raise ValueError(f"Invalid duration: {ffmpeg_duration}")


@functools.cache
def _audio_file_information_cached(file_name: str, file_size: int, modification_date: float) -> dict:
    """
    Low-level function to get audio file information (duration, bit rate) using `ffprobe`. The function calls
    are being cached to increase run time efficiency. To prevent returning cached data on modified files, the
    file_size and modification date must be provided it as well.

    This function is not intended to be called directly. Use `audio_file_information` instead.

    Parameters
    ----------
        file_name: str
            name of the audio file to be probed
        file_size: int
            size of the file
        modification_date:

    Returns
    -------
        dictionary with file information.
    """
    cmd = [
        f"{FFPROBE_BINARY}",
        "-v",
        "error",
        "-show_entries",
        "format=duration:stream=bit_rate",
        "-of",
        "default=noprint_wrappers=1",
        "-sexagesimal",
        file_name,
    ]
    result = subprocess.run(cmd, capture_output=True).stdout.decode("utf-8").split("\n")

    file_info = {key: value for key, value in (line.strip().partition("=")[::2] for line in result) if key.strip()}
    file_info["duration"] = duration_parser(file_info["duration"])

    for key, value in file_info.items():
        try:
            num_value = int(value)
        except ValueError:
            try:
                num_value = float(value)
            except ValueError:
                continue
        except TypeError:
            continue

        file_info[key] = num_value

    return file_info


def audio_file_information(file_name: str) -> dict:
    """
    Function to get audio file information (duration, bit rate) using `ffprobe`.

    Parameters
    ----------
    file_name: str
        name of the file to be probed

    Returns
    -------
        dict of information
    """

    file_name_path = Path(file_name).absolute()

    if file_name_path.is_file():
        info = _audio_file_information_cached(
            str(file_name_path),
            file_name_path.stat().st_size,
            file_name_path.stat().st_mtime,
        )
    else:
        raise FileNotFoundError(f"File {file_name} was not found.")

    return info


def bit_rate(filename: str) -> int:
    """
    Get the bit_rate of an audio file.
    Parameters
    ----------
    filename: str

    Returns
    -------

    """
    return audio_file_information(filename)["bit_rate"]


def duration(filename: str) -> timedelta:
    """
    Get the duration of an audio file as datetime.time.
    """

    return audio_file_information(filename)["duration"]


class AudioSequence(pydantic.BaseModel):
    filename: str
    start_time: Optional[timedelta] = None
    end_time: Optional[timedelta] = None

    @pydantic.computed_field
    def file_duration(self) -> timedelta:
        return duration(self.filename)

    @pydantic.computed_field()
    def bit_rate(self) -> int:
        return bit_rate(self.filename)

    @pydantic.computed_field
    def sequence_duration(self) -> timedelta:
        return (self.file_duration if self.end_time is None else self.end_time) - (
            self.start_time if self.start_time is not None else timedelta(0)
        )

    @pydantic.field_validator("start_time", mode="before")
    @classmethod
    def validate_start_time(cls, value):
        if value is None or value == "":
            return None

        if isinstance(value, str):
            value = duration_parser(value)

        if value.total_seconds() < 0:
            raise ValueError(f"Start time must be positive. Instead it is {value}.")

        return value

    @pydantic.field_validator("end_time", mode="before")
    @classmethod
    def validate_end_time(cls, value):
        if value is None or value == "":
            return None

        if isinstance(value, str):
            value = duration_parser(value)

        return value

    #
    @pydantic.field_validator("filename")
    def validate_filename(cls, value):
        if not Path(value).is_file():
            raise ValueError(f"File {value} does not exist.")

        return value

    #
    # @pydantic.model_validator(mode="after")
    # def check_times(self) -> Self:
    #     if self.start_time is None or self.end_time is None:
    #         # start and end time are not set so there is nothing to do.
    #         return self
    #
    #     if self.start_time is not None and self.start_time > self.duration:
    #         raise ValueError(f"Start time must be less than duration. Instead it is {self.start_time}.")
    #
    #     if self.end_time is not None and self.end_time > self.duration:
    #         raise ValueError(f"End time must be less than duration. Instead it is {self.end_time}.")
    #
    #     if self.start_time is not None and self.end_time is not None and self.start_time > self.end_time:
    #         raise ValueError(f"End time must be greater than start time. Instead it is {self.end_time}.")

    def to_ffmpeg_args(self) -> list[str]:
        ffmpeg_args = []
        if self.start_time is not None:
            ffmpeg_args.extend(["-ss", f"{self.start_time}"])

        if self.end_time is not None:
            ffmpeg_args.extend(["-to", f"{self.end_time}"])

        ffmpeg_args.extend(["-i", f"{self.filename}"])

        return ffmpeg_args


def audio_sequences_to_ffmpeg_args(audio_sequences: list[AudioSequence]) -> list[str]:
    """
    Convert a list of audio sequences to arg string for ffmpeg call.
    Parameters
    ----------
    audio_sequences

    Returns
    -------
    list[str]
    """
    ffmpeg_args = []

    for audio_sequence in audio_sequences:
        ffmpeg_args.extend(audio_sequence.to_ffmpeg_args())

    return ffmpeg_args


def ffmpeg_cmd_for_merging(
    audio_sequences: list[AudioSequence],
    output_filename: str,
    bit_rate: int | str = None,
    hide_banner: bool = True,
    overwrite_output: bool = True,
    additional_ffmpeg_args: list[str] = None,
) -> list[str]:
    if additional_ffmpeg_args is None:
        additional_ffmpeg_args = []
    else:
        if hide_banner:
            try:
                additional_ffmpeg_args.remove("-hide_banner")
            except ValueError:
                pass

        if overwrite_output is True:
            try:
                additional_ffmpeg_args.remove("-y")
            except ValueError:
                pass
        elif overwrite_output is False:
            try:
                additional_ffmpeg_args.remove("-n")
            except ValueError:
                pass

    input_args = audio_sequences_to_ffmpeg_args(audio_sequences)

    if len(audio_sequences) > 1:
        input_streams = "".join([f"[{index}:a]" for index in range(len(audio_sequences))])
        output_args = [
            "-filter_complex",
            f"{input_streams}concat=n={len(audio_sequences)}:v=0:a=1",
        ]

        if bit_rate is not None:
            if bit_rate == 0:
                bit_rate = max([x.bit_rate for x in audio_sequences])

            output_args.extend(["-b:a", f"{bit_rate}"])

    elif len(audio_sequences) == 1:
        output_args = ["-c:a", "copy"]
    else:
        return []

    output_args.extend(additional_ffmpeg_args)
    output_args.append(f"{output_filename}")

    base_command = [FFMPEG_BINARY]
    if hide_banner:
        base_command += ["-hide_banner"]
    if overwrite_output:
        base_command += ["-y"]
    else:
        base_command += ["-n"]

    ffmpeg_cmd = base_command + input_args + output_args

    return ffmpeg_cmd


def progress_from_ffmpeg_output(line) -> timedelta | None:
    """
    This functions returns the progress of the ffmpeg process.

    Parameters
    ----------
    line: str
        output_line from ffmpeg merge process

    Returns
    -------
        None if merge process has not yet started otherwise the progress as time position of the merging.
    """

    match = FFMPEG_PROGRESS_RE.match(line)

    if not match:
        return None

    position = duration_parser(match.group(2))

    return position


class FFMpegProgressFraction:
    """
    Class to calculate the progress as a fraction of the FFMpeg merge process.
    """

    def __init__(self, audio_sequence: list[AudioSequence]):
        """
        audio_sequence is the list of audio_sequences for merging.
        Parameters
        ----------
        audio_sequence: list[AudioSequence]
        """

        super().__init__()

        self.merged_duration = sum((x.sequence_duration for x in audio_sequence), timedelta(0))

    def __call__(self, line: str) -> float:
        """
        Returns the progress as fraction from the current output line.

        Parameters
        ----------
        line: str
            current output line from ffmpeg merge process.

        Returns
        -------
            progress: float
        """

        time_position = progress_from_ffmpeg_output(line)

        if time_position is None:
            return float("nan")

        progress = time_position.total_seconds() / self.merged_duration.total_seconds()

        return progress
