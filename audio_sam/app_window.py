import datetime
from pathlib import Path

import gi
import pydantic

from gobject_model import AudioSequenceGObject
from merge_progress_window import MergeWindow

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, Gio, Gdk  # noqa: E402

from audio_library import AudioSequence  # noqa: E402


class AudioSamWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        super().__init__(application=app, title="Audio sequence merger")
        self.props.hexpand = True
        self.props.vexpand = True

        content = Gdk.ContentFormats.new_for_gtype(Gdk.FileList)
        self.drop_target = Gtk.DropTarget(formats=content, actions=Gdk.DragAction.COPY)
        self.drop_target.connect("drop", self._on_drop)

        self.add_controller(self.drop_target)

        self.model = Gio.ListStore(item_type=AudioSequenceGObject)

        self.column_view = Gtk.ColumnView(model=Gtk.SingleSelection(model=self.model))
        self.column_view.props.hexpand = True
        self.column_view.props.vexpand = True

        for title, prop in (
            ("Filename", "filename"),
            ("Start time", "start_time"),
            ("End time", "end_time"),
        ):
            factory = Gtk.SignalListItemFactory()
            factory.connect("setup", self._on_factory_setup, prop)
            factory.connect("bind", self._on_factory_bind, prop)

            column = Gtk.ColumnViewColumn(title=title, factory=factory)
            column.props.expand = True

            self.column_view.append_column(column)

        box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=12,
            valign=Gtk.Align.FILL,
            halign=Gtk.Align.FILL,
        )

        box.props.margin_start = 12
        box.props.margin_end = 12
        box.props.margin_top = 6
        box.props.margin_bottom = 6
        box.append(self.column_view)
        box.props.vexpand = True
        box.props.hexpand = True

        hbox = Gtk.Box(spacing=12, valign=Gtk.Align.CENTER, halign=Gtk.Align.CENTER)
        self.up_button = Gtk.Button(label="Move up")
        self.up_button.connect("clicked", self._move_up)

        self.down_button = Gtk.Button(label="Move Down")
        self.down_button.connect("clicked", self._move_down)

        self.delete_button = Gtk.Button(label="Delete File")
        self.delete_button.connect("clicked", self._delete_entry)

        self.clear_button = Gtk.Button(label="Clear list")
        self.clear_button.connect("clicked", self._clear_all)

        self.add_file = Gtk.Button(label="Add a file")
        self.add_file.connect("clicked", self._add_file)
        self.add_file_dialog = Gtk.FileChooserNative.new(
            title="Add a file", parent=self, action=Gtk.FileChooserAction.OPEN
        )
        self.add_file_dialog.connect("response", self._add_file_response)

        hbox.append(self.add_file)
        hbox.append(self.up_button)
        hbox.append(self.down_button)
        hbox.append(self.delete_button)
        hbox.append(self.clear_button)

        box.append(hbox)

        self.save_button = Gtk.Button(label="Save to ...", halign=Gtk.Align.CENTER)
        self.save_dialog = Gtk.FileChooserNative.new(
            title="Save to ...", parent=self, action=Gtk.FileChooserAction.SAVE
        )
        self.save_dialog.connect("response", self._save_dialog_response)
        self.save_button.connect("clicked", self._save_to)
        box.append(self.save_button)

        self.run_button = Gtk.Button(label="Run", halign=Gtk.Align.CENTER)
        self.run_button.connect("clicked", self._on_run)
        box.append(self.run_button)

        self.set_child(box)

        self.merge_future = None

    def _on_drop(self, drop_target, value: Gdk.FileList, x, y, user_data=None):
        file_list: list[Gio.File] = value.get_files()

        for file in file_list:
            self.add_file_from_name(file.get_path())

    def _move_up(self, button):
        if len(self.model) < 2:
            return

        selected = [
            (index, item) for index, item in enumerate(self.model) if self.column_view.get_model().is_selected(index)
        ]

        for index, item in selected:
            self.model.remove(index)
            self.model.insert(index - 1, item)

            self.column_view.get_model().unselect_item(index)
            self.column_view.get_model().select_item(index - 1, False)

    def _move_down(self, button):
        if len(self.model) < 2:
            return

        selected = [
            (index, item) for index, item in enumerate(self.model) if self.column_view.get_model().is_selected(index)
        ]

        for index, item in selected:
            self.model.remove(index)
            self.model.insert(index + 1, item)

            self.column_view.get_model().unselect_item(index)
            self.column_view.get_model().select_item(index + 1, False)

    def _clear_all(self, button):
        self.model.remove_all()

    def _delete_entry(self, button):
        index_to_remove = []
        for index, item in enumerate(self.model):
            if self.column_view.get_model().is_selected(index):
                index_to_remove.append(index)

        for index in reversed(index_to_remove):
            self.model.remove(index)

    def _add_file(self, button):
        self.add_file_dialog.show()

    def _add_file_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            file = dialog.get_file()
            filename = file.get_path()

            self.add_file_from_name(filename)

    def add_file_from_name(self, filename):
        try:
            audio = AudioSequence(filename=filename)
        except pydantic.ValidationError as e:
            dialog = Gtk.AlertDialog()
            dialog.set_message("Invalid filename")
            dialog.set_detail(str(e))
            dialog.set_modal(True)
            dialog.set_buttons(["Ok"])
            dialog.choose(self)
        else:
            store_item = AudioSequenceGObject(
                filename=str(audio.filename),
                start_time=str(audio.start_time) if audio.start_time else "",
                end_time=audio.end_time if audio.end_time else "",
            )

            self.model.append(store_item)

    def _save_to(self, button):
        self.save_dialog.show()

    def _save_dialog_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            file = dialog.get_file()
            filename = file.get_path()

            self.save_button.set_label(filename)

    def _on_run(self, button):
        audio_sequence = [
            AudioSequence(
                filename=item.filename,
                start_time=item.start_time if item.start_time else None,
                end_time=item.end_time if item.end_time else None,
            )
            for item in self.model
        ]

        if not audio_sequence:
            return

        if self.save_button.get_label() == "Save to ...":
            output = Path(audio_sequence[0].filename).parent / "output.mp3"
            while output.is_file():
                output = output.with_stem(f"output-{datetime.datetime.now().strftime('%Y%m%d-%H%M%S')}")
        else:
            output = Path(self.save_button.get_label())

        if output.suffix == "":
            output = output.with_suffix(Path(audio_sequence[0].filename).suffix)

        merge_progress = MergeWindow(audio_sequence, output)
        merge_progress.props.transient_for = self
        merge_progress.present()

        merge_progress.start()

    @staticmethod
    def _on_factory_setup(factory, list_item, prop):
        if prop == "filename":
            cell = Gtk.Label()
        else:
            cell = Gtk.EditableLabel()
        list_item.set_child(cell)

    @staticmethod
    def _on_factory_bind(factory, list_item, prop):
        cell = list_item.get_child()
        store_item = list_item.get_item()
        cell.set_text(store_item.get_property(prop))
        if prop != "filename":
            cell.bind_property("text", store_item, prop)
