from gi.repository import GObject


class AudioSequenceGObject(GObject.Object):
    __gtype_name__ = "AudioSequenceGObject"

    filename = GObject.Property(type=str)
    start_time = GObject.Property(type=str)
    end_time = GObject.Property(type=str)

    def __init__(self, filename, start_time, end_time):
        super().__init__()

        self.filename = filename
        self.start_time = start_time
        self.end_time = end_time

    def __repr__(self):
        return f"AudioSequenceModel({self.filename}, {self.start_time}, {self.end_time})"
