Audio Split and Merge
=====================

A simple frontend to merge several audio files into a single file.

Description
-----------

Audio Split and Merge serves a simple purpose: make it easy to merge parts of several audio files into a new file. 
The heavy lifting is done by ffmpeg. Audio split and merge serves as a simple frontend for this purpose.


Installation
------------
This project depends on Python, GTK4 and it's Python bindings (pygobject & pycairo), pydantic
and ffmpeg.
The easiest way to get everything installed is to use Anaconda:

#. If not already installed install `Anaconda <https://docs.anaconda.com/free/anaconda/install>`_,
   `Miniconda <https://docs.anaconda.com/free/miniconda/>`_ or
   `Mamba <https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html>`_
#. Create a new environment:
   ``conda create -n audio-sam``
#. Install necessary packages:
   ``conda install -n audio-sam python>=3.11 gtk4>=4.10 pycairo pygobject pydantic>=2.0 ffmpeg``

#. Download and extract the source code or clone the repository into a local folder
#. Install the project using setuptools
#. If ffmpeg is not installed, install ffmpeg on your system

Usage
-----

After installation run "audio-sam" to start the GUI. 

Support
-------
In case you find a bug or have suggestions for improvements create an issue

Author and acknowledgement
--------------------------
This project was inspired by https://superuser.com/questions/587511/concatenate-multiple-wav-files-using-single-command-without-extra-file. 

License
-------
This project is licensed under MIT License.
